// ## Завдання

// 1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата).
// Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// 2. Створіть гетери та сеттери для цих властивостей.

class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(newName) {
        if (!newName) {
            throw Error('Please enter name in correct format');
        }
        if (!isNaN(newName)) {
            throw Error('Name should not be a number');
        }
        newName = newName.trim();
        this._name = newName;
    }
    get name() {
        return this._name;
    }

    set age(newAge) {
        if (!newAge) {
            throw Error('Please enter age in correct format');
        }
        if (isNaN(newAge)) {
            throw Error('Age should be a number');
        }
        this._age = newAge;
    }
    get age() {
        return this._age;
    }

    set salary(newSalary) {
        if (!newSalary) {
            throw Error('Please enter age in correct format');
        }
        if (isNaN(newSalary)) {
            throw Error('Age should be a number');
        }
        this._salary = newSalary;
    }
    get salary() {
        return this._salary;
    }
}


const employee = new Employee('Jack Smith', '26', '25000');

employee.name = 'Mike Cooper';
console.log(employee.name);

employee.age = '5';
console.log(employee.age);

employee.salary = '50000';
console.log(employee.salary);

// 3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// 4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.

class Programmer extends Employee {
    #lang;
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.#lang = lang;
    }

    set lang(newLang) {
        this.#lang = newLang;
    }
    get lang() {
        return this.#lang;
    }

    get salary() {
        return this._salary * 3;
    }
}

const programmer = new Programmer('Jack Simpson', '25', '30000', ['JS', 'PHP']);
console.log(programmer);
console.log(programmer.salary);

// 5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

const programmer1 = new Programmer('Alice Smith', '35', '4000', 'JS');
programmer1.lang = ['JS', 'Python', 'PHP'];
console.log(programmer1);